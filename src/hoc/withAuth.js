import Spinner from "../components/reusable/Spinner/Spinner";
import { useContext } from "react";
import { getUser } from "../components/main/login/UsersAPI";
import { AppContext } from "../contextAPI/AppProvider";
import { getLocalStorageItem } from "../utils/localStorage";

const withAuth = (Component) => (props) => {
  const [state, dispatch] = useContext(AppContext);
  const user = getLocalStorageItem("user");

  // If localStorage user exists and the contextAPI is empty
  if (user != null) {
    if (state.username == null) {
      getUser(user.name).then((response) =>
        dispatch({
          type: "translations:GET_ALL",
          payload: response.translations
        })
      );
      dispatch({ type: "user:LOGIN", payload: [user.id, user.name] });
    }
    return <Component {...props} />;
  } else {
    // If no localStorage user exists, redirect to Spinner component which goes to Login
    return <Spinner />;
  }
};

export default withAuth;

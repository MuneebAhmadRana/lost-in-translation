import { Table } from "semantic-ui-react";

function TranslationHistory({ translations }) {
  translations = translations.splice(0, 10);
  const translationRows = translations.map((translation, index) => {
    index = index + 1;
    return (
      <Table.Row key={index}>
        <Table.Cell>{index}</Table.Cell>
        <Table.Cell>{translation}</Table.Cell>
      </Table.Row>
    );
  });
  return (
    <div>
      <div id="translation-table-div">
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>No.</Table.HeaderCell>
              <Table.HeaderCell>Translation</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>{translationRows}</Table.Body>
        </Table>
      </div>
    </div>
  );
}

export default TranslationHistory;

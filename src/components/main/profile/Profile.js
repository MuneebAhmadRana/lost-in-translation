import TranslationHistory from "./TranslationHistory";
import withAuth from "../../../hoc/withAuth";
import LogoutButton from "../../reusable/LogoutButton/LogoutButton";
import { useContext, useState, useEffect } from "react";
import { AppContext } from "../../../contextAPI/AppProvider";
import { deleteTranslations } from "../translation/TranslationsAPI";
import { Redirect } from "react-router-dom";
import { Button } from "semantic-ui-react";
import { getUser } from "../login/UsersAPI";
import { getLocalStorageItem } from "../../../utils/localStorage";
import "./Profile.css";

function Profile() {
  const [state, dispatch] = useContext(AppContext);
  const [redirect, setRedirect] = useState(null);

  useEffect(() => {
    const user = getLocalStorageItem("user");
    getUser(user.name).then((response) => {
      dispatch({
        type: "translations:GET_ALL",
        payload: response.translations
      });
    });
  }, []);
  function onDeleteTranslationsClick() {
    deleteTranslations(state.userID);
    dispatch({ type: "translations:DELETE_ALL", payload: [] });
  }

  function redirectTranslate() {
    setRedirect("/translate");
  }

  function renderTable() {
    if (state.translations.length <= 0) {
      return (
        <h3 id="no-translation-message">There are no translations to show</h3>
      );
    }
    return (
      <div id="translations-delete">
        <TranslationHistory
          translations={state.translations}
        ></TranslationHistory>
        <Button id="delete-button" onClick={onDeleteTranslationsClick} fluid>
          Delete Translations
        </Button>
      </div>
    );
  }

  if (redirect != null) {
    return <Redirect to={redirect} />;
  }

  const render = renderTable();
  return (
    <div>
      <div id="profile-translate-div">
        <h1 id="profile-header">{state.username}'s Profile</h1>

        <Button id="translate-button" onClick={redirectTranslate}>
          Lets Translate
        </Button>
      </div>
      {render}

      <LogoutButton buttonId="logout-button" />
    </div>
  );
}

export default withAuth(Profile);

import SearchBar from "../../reusable/SearchBar/SearchBar";
import { useContext, useState, useEffect } from "react";
import { getUser, createUser } from "./UsersAPI";
import { Redirect } from "react-router-dom";
import { AppContext } from "../../../contextAPI/AppProvider";
import { renderNotification } from "../../reusable/Notification/Notification";
import {
  getLocalStorageItem,
  setLocalStorageItem
} from "../../../utils/localStorage";
import "./Login.css";

function Login() {
  //Global State
  const [, dispatch] = useContext(AppContext);

  //Local State
  const [redirect, setRedirect] = useState(null);
  const [notification, setNotification] = useState(null);

  useEffect(() => {
    if (!getLocalStorageItem("user")) {
      return;
    }
    const user = getLocalStorageItem("user");
    dispatch({
      type: "user:LOGIN",
      payload: [user.id, user.name]
    });
    setRedirect("/translate");
  }, []);

  // Function linked with Login button.
  async function handleLoginSubmit(username) {
    try {
      if (username === "") {
        setNotification("loginEmpty");
        return;
      }
      const user = await getUser(username);
      if (user == null) {
        setNotification("loginFailure");
        return;
      }
      const translations = user.translations;
      setLocalStorageItem("user", user.username, user.id, 900000);
      // Setting contextAPI values.
      dispatch({ type: "user:LOGIN", payload: [user.id, user.username] });
      dispatch({ type: "translations:GET_ALL", payload: translations });
      // Setting Success Notification
      setNotification("loginSuccess");

      // Waiting 2.1 seconds before redirecting to /translate
      setTimeout(() => {
        setRedirect("/translate");
      }, 2100);
    } catch (error) {
      console.log(error);
    }
  }

  // Function linked with sign in button
  async function handleSignUpSubmit(username) {
    try {
      // Check for if input is empty
      if (username === "") {
        setNotification("signupEmpty");
        return;
      }
      const user = await getUser(username);
      // Check if user already exists in database.
      if (user) {
        setNotification("signupFailure");
        return;
      }
      createUser(username);
      setNotification("signupSuccess");
    } catch (error) {
      console.log(error);
    }
  }

  switch (notification) {
    case "loginSuccess":
      renderNotification("login-notification-div", "loginSuccess");
      break;
    case "loginFailure":
      renderNotification("login-notification-div", "loginFailure");
      break;
    case "loginEmpty":
      renderNotification("login-notification-div", "loginEmpty");
      break;
    case "signupSuccess":
      renderNotification("signup-notification-div", "signupSuccess");
      break;
    case "signupFailure":
      renderNotification("signup-notification-div", "signupFailure");
      break;
    case "signupEmpty":
      renderNotification("signup-notification-div", "signupEmpty");
      break;
    default:
      break;
  }

  if (redirect != null) {
    return <Redirect to={redirect} />;
  }
  return (
    <div id="login-signup-div">
      <div id="login-div">
        <h1>Login</h1>
        <SearchBar
          buttonText="Login"
          inputId="login-input"
          buttonId="login-button"
          placeholder="Let's get Translating! 🐱‍🚀"
          onSubmit={handleLoginSubmit}
        ></SearchBar>
        <div id="login-notification-div"></div>
      </div>
      <br></br>
      <div id="signup-div">
        <h1>Signup </h1>
        <SearchBar
          buttonText="Signup"
          inputId="signup-input"
          buttonId="signup-button"
          placeholder="Add your name here to create an account 🐱‍👤"
          onSubmit={handleSignUpSubmit}
        ></SearchBar>
        <div id="signup-notification-div"></div>
      </div>
    </div>
  );
}

export default Login;

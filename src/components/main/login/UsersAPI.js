import axios from "axios";

const BASE_URL = "http://localhost:5000";

const api = axios.create({
  baseURL: BASE_URL
});

export async function getUser(username) {
  const user = await api.get("/users", {
    params: {
      username: username
    }
  });
  if (!user) {
    return null;
  }

  return user.data[0];
}

export function createUser(username) {
  api
    .post("/users", {
      username: username,
      translations: []
    })
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error);
    });
}

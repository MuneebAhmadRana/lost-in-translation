import SearchBar from "../../reusable/SearchBar/SearchBar";
import SignList from "./SignList";
import withAuth from "../../../hoc/withAuth";
import { AppContext } from "../../../contextAPI/AppProvider";
import { addTranslation } from "./TranslationsAPI";
import { Redirect } from "react-router-dom";
import { useContext, useState } from "react";
import { renderNotification } from "../../reusable/Notification/Notification";
import { Button } from "semantic-ui-react";
import "./Translator.css";

function Translator() {
  //Global State
  const [state, dispatch] = useContext(AppContext);

  //Local State
  const [translation, setTranslation] = useState("");
  const [redirect, setRedirect] = useState(null);
  const regExp = /^[a-z][a-z_ ]*$/i;

  async function handleTranslateSubmit(translation) {
    if (translation.split("").length <= 0) {
      renderNotification("translate-notification-div", "translateEmpty");
      return;
    } else if (!regExp.test(translation)) {
      renderNotification("translate-notification-div", "translateBadCharacter");
      return;
    } else {
      // Translation added to contextAPI translations
      dispatch({ type: "translations:ADD", payload: translation });

      //Translation added to database
      addTranslation(state.translations, state.userID);

      setTranslation(translation);
    }
  }
  function onViewProfileClick() {
    setRedirect("/profile");
  }

  if (redirect != null) {
    return <Redirect to={redirect} />;
  }
  return (
    <div>
      <div id="username-profile-div">
        <h1 id="welcome-name">Welcome {state.username}</h1>
        <Button id="profile-button" onClick={onViewProfileClick}>
          View Profile
        </Button>
      </div>
      <SearchBar
        inputId="translator-input"
        buttonId="translator-button"
        buttonText="Translate"
        placeholder="Type text to Translate 🐱‍💻"
        onSubmit={handleTranslateSubmit}
      ></SearchBar>
      <div id="translate-notification-div"></div>
      <SignList id="signs-div" translation={translation}></SignList>
    </div>
  );
}

export default withAuth(Translator);

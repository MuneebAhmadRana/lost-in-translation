const Sign = ({ letter, alternate }) => {
  /*  const source = require(`./static/images/` + letter + `.png`); */
  return (
    <img
      className="sign"
      src={require(`../../../assets/signs/${letter}.png`).default}
      alt={alternate}
    />
  );
};
export default Sign;

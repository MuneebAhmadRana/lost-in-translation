import Sign from "./Sign";

const SignList = ({ translation }) => {
  translation = translation.replace(/ /g, "").toLowerCase().split("");
  const images = translation.map((letter, index) => {
    return (
      <Sign letter={`${letter}`} alternate={`${index}`} key={`${index}`}></Sign>
    );
  });
  return <div id="signs-div">{images}</div>;
};
export default SignList;

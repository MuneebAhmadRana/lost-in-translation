import { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";

import { Dimmer, Loader, Segment, Image } from "semantic-ui-react";
const Spinner = () => {
  const [redirect, setRedirect] = useState(null);

  useEffect(() => {
    setTimeout(() => {
      setRedirect("/");
    }, 1500);
  }, []);

  if (redirect != null) {
    return <Redirect to={redirect} />;
  }

  return (
    <>
      <Segment>
        <Dimmer active inverted>
          <Loader>Login required</Loader>
        </Dimmer>

        <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" />
      </Segment>
    </>
  );
};

export default Spinner;

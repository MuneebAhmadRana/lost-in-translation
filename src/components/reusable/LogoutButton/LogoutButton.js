import { useState, useContext } from "react";
import { Redirect } from "react-router-dom";
import { Button } from "semantic-ui-react";
import { AppContext } from "../../../contextAPI/AppProvider";
import { deleteLocalStorageItem } from "../../../utils/localStorage";

function LogoutButton({ buttonId }) {
  const [, dispatch] = useContext(AppContext);
  const [redirect, setRedirect] = useState(null);
  function onLogoutButtonClick(event) {
    event.preventDefault();
    deleteLocalStorageItem("user");
    dispatch({ type: "user:LOGOUT" });
    setRedirect("/");
  }

  if (redirect != null) {
    return <Redirect to={redirect} />;
  }

  return (
    <Button id={buttonId} onClick={onLogoutButtonClick} color="red" fluid>
      Logout
    </Button>
  );
}

export default LogoutButton;

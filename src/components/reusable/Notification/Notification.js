import ReactDOMServer from "react-dom/server";

const outcomes = {
  loginSuccess: {
    id: "login-success",
    message: "Successfuly logged in. Please wait as we redirect you."
  },
  loginFailure: {
    id: "login-failure",
    message: "Login Failed. Please try again."
  },
  loginEmpty: {
    id: "login-empty",
    message: "Please  provide a username to login"
  },
  signupSuccess: {
    id: "signup-success",
    message: "Sign up successful. Please login with your new username."
  },
  signupFailure: {
    id: "signup-failure",
    message: "Sign up failed. The username already exists"
  },
  signupEmpty: {
    id: "singup-empty",
    message: "Please provide a username to sign up"
  },
  translateEmpty: {
    id: "translate-empty",
    message: "Please write something before trying to translate"
  },
  translateBadCharacter: {
    id: "translate-bad-character",
    message: "Please only use English alphabets"
  }
};

export function renderNotification(divID, notification) {
  const elNotificationDiv = document.getElementById(divID);
  elNotificationDiv.innerHTML = ReactDOMServer.renderToStaticMarkup(
    <Notification type={notification} />
  );
  setTimeout(() => {
    elNotificationDiv.innerHTML = "";
  }, 2000);
}

const Notification = ({ type }) => {
  const result = outcomes[type];
  return (
    <div id={result.id} className="notification-box">
      <h3>{result.message}</h3>
    </div>
  );
};

export default Notification;

import "./Navbar.css";
import Logo from "../../../assets/logos/Logo.png";
const Navbar = () => {
  return (
    <div className="navbar">
      <img id="navbar-image" src={Logo} alt="Logo.png" />

      <div className="navbar-header">Lost In Translation!</div>
    </div>
  );
};

export default Navbar;

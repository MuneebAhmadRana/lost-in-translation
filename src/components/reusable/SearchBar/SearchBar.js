import React, { useState } from "react";
import { Button, Input, Form } from "semantic-ui-react";
import "./SearchBar.css";
const SearchBar = ({
  placeholder,
  buttonText,
  onSubmit,
  inputId,
  buttonId
}) => {
  const [term, setTerm] = useState("");
  const onFormSubmit = (e) => {
    e.preventDefault();
    onSubmit(term);
  };

  return (
    <Form>
      <Input
        id={inputId}
        fluid
        icon="arrow circle right purple big"
        size="big"
        placeholder={placeholder}
        type="text"
        value={term}
        onChange={(e) => {
          setTerm(e.target.value);
        }}
        onSubmit={onFormSubmit}
      ></Input>
      <Button
        id={buttonId}
        content={buttonText}
        fluid
        type="submit"
        onClick={onFormSubmit}
      ></Button>
    </Form>
  );
};

export default SearchBar;

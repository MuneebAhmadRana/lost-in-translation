import AppReducer from "./AppReducer";
import { createContext, useReducer } from "react";
import { initialState } from "./AppReducer";

export const AppContext = createContext();

function AppProvider(props) {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  return (
    <AppContext.Provider value={[state, dispatch]}>
      {props.children}
    </AppContext.Provider>
  );
}

export default AppProvider;

export const ACTION_ADD_TRANSLATION = "translations:ADD";
export const ACTION_GET_USER_TRANSLATIONS = "translations:GET_ALL";
export const ACTION_DELETE_USER_TRANSLATIONS = "translations:DELETE_ALL";
export const ACTION_USER_LOGIN = "user:LOGIN";
export const ACTION_USER_LOGOUT = "user:LOGOUT";

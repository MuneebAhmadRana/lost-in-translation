import {
  ACTION_ADD_TRANSLATION,
  ACTION_DELETE_USER_TRANSLATIONS,
  ACTION_GET_USER_TRANSLATIONS,
  ACTION_USER_LOGIN,
  ACTION_USER_LOGOUT
} from "./actions";

export const initialState = {
  userID: null,
  username: null,
  translations: []
};

function AppReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_GET_USER_TRANSLATIONS:
      return {
        ...state,
        translations: action.payload
      };
    case ACTION_ADD_TRANSLATION:
      return {
        ...state,
        translations: [...state.translations, action.payload]
      };
    case ACTION_DELETE_USER_TRANSLATIONS:
      return {
        ...state,
        translations: action.payload
      };
    case ACTION_USER_LOGIN:
      return {
        ...state,
        userID: action.payload[0],
        username: action.payload[1]
      };
    case ACTION_USER_LOGOUT:
      return {
        ...state,
        userID: null,
        username: null,
        translations: []
      };

    default:
      return state;
  }
}

export default AppReducer;

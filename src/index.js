import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";
import AppProvider from "./contextAPI/AppProvider";
import "semantic-ui-css/semantic.min.css";

ReactDOM.render(
  <AppProvider>
    <App />
  </AppProvider>,
  document.getElementById("root")
);

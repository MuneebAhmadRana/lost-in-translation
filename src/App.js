import React from "react";
import Login from "../src/components/main/login/Login";
import Translator from "../src/components/main/translation/Translator";
import Profile from "../src/components/main/profile/Profile";
import Navbar from "./components/reusable/Navbar/Navbar";
import { Container } from "semantic-ui-react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <Router>
      <Router>
        <Navbar></Navbar>
        <main>
          <Container>
            <Switch>
              <Route exact path="/" component={Login}></Route>
              <Route path="/translate" component={Translator}></Route>
              <Route path="/profile" component={Profile}></Route>
            </Switch>
          </Container>
        </main>
      </Router>
    </Router>
  );
}

export default App;

export function setLocalStorageItem(key, name, id, ttl) {
  const now = new Date();
  const item = {
    name: name,
    id: id,
    expiry: now.getTime() + ttl
  };
  // Converting to JSON since localStorage only contains strings
  localStorage.setItem(key, JSON.stringify(item));
}

export function getLocalStorageItem(key) {
  const itemString = localStorage.getItem(key);
  if (!itemString) {
    return null;
  }
  const item = JSON.parse(itemString);
  const now = new Date();
  // Checks if the time in the localStorage is less than the current time, if so, removes the localStorage user
  if (now.getTime() > item.expiry) {
    localStorage.removeItem(key);
    return null;
  }
  return item;
}

export function deleteLocalStorageItem(key) {
  const item = localStorage.getItem(key);
  if (!item) {
    return null;
  }
  localStorage.removeItem(key);
}

//commonJS vs ES6
// import {something} from "somewhere"; --> ES6
// requrie("") ---> commonJS
const jsonServer = require("json-server");
const server = jsonServer.create();
const router = jsonServer.router("db.json");
const middleware = jsonServer.defaults(); //takes all the middleware needed
const { PORT = 5000 } = process.env;

server.use(middleware);

server.use(router);

server.listen(PORT, () => {
  console.log(`Running on Port: ${PORT}`);
});
